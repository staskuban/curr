import {Config} from "../config/config";

/**
 * Базовый класс http основан на fetch API
 */
export class HttpBaseControler {
    constructor(config = Config.config) {
        this.config = config
    }

    /**
     * GET
     * @param query -адрес запроса относительно entry point
     * @param settings - дополнительные параметры (не используется)
     * @returns {Promise<Response>}
     */
    async get(query, settings) {
        return await fetch(`${this.config.api_entry}${query}`)
    }
}