import {HttpBaseControler} from "./http.base.controller";

export class CurrencyController {
    constructor() {
        this.http = new HttpBaseControler();
    }

    /**
     * Получает ответ запроса и вычленяет body
     * @returns {Promise<*>}
     */
    async getCurrList() {
        let data = await  this.http.get('/stocks.json');
        try {
            return await data.json();
        }
        catch (err) {
            console.error(err);
        }
    }
}