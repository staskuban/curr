/**
 * конфиги приложения
 */
export class Config {
    /**
     * возвращает конфиг приложения
     * @returns {{api_entry: string, appVersion: string}}
     */
    static get config(){
        return {
            api_entry:'http://phisix-api3.appspot.com/',
            appVersion:'0.1',
        }
    }

    /**
     * set value accessor запрещает редактирование и изменение конфига
     * @param data
     */
    static set config(data){
        console.error(`Config is read-only inserting data ${data} was skipped`)
    }
}