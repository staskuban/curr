import React from 'react';
import {
    Container,
    Header,
    Content,
    Body,
    Title,
    Root,
    Right,
    Icon,
    Button,
    Spinner,
    List,
    ListItem,
    Text
} from 'native-base';
import {StyleSheet, FlatList} from "react-native";
import {CurrencyController} from "../controller/currency.controller";
import {Cell, Row} from 'react-native-data-table';

export class CurrencyListPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {loading: true};
        this.currencyList = []
        this.currencyCtrl = new CurrencyController()
    }

    componentDidMount() {
        this.fillCurrList()
        this._interval = setInterval(this.fillCurrList.bind(this), 25000);

    }

    /**
     * Заполняет массив с валютами
     * @param currency - контроллер валют
     * @returns {undefined}
     */
    async fillCurrList() {

        this.setState({loading: true});
        try {
            /**
             * Промежуточный результат запроса (точнее промайс из response.json())
             * @type {*}
             */
            let result = await this.currencyCtrl.getCurrList();
            this.currencyList = [...result.stock];
        }
        catch (err) {
            console.error(error)
        }
        finally {
            this.setState({loading: false});
        }


    }

    keyExtractor = (item, index) => index + item.code;

    renderItem = ({item}) => (
        <Row style={styles.Row}>
            <Cell>{item.name}</Cell>
            <Cell>{item.price.amount.toFixed(2)}</Cell>
            <Cell>{item.volume}</Cell>
        </Row>
    );

    render() {
        return (
            <Container>
                <Header style={styles.header}>
                    <Body>
                    <Title>Currencya</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => {
                            this.fillCurrList()
                        }}>
                            <Icon name='refresh'/>
                        </Button>
                    </Right>
                </Header>
                <Content>
                    {this.state.loading ?
                        <Spinner color='blue'/> :
                        <FlatList data={this.currencyList}
                                  renderItem={this.renderItem}
                                  keyExtractor={this.keyExtractor}/>
                    }

                </Content>
            </Container>


        )
    }
}

const styles = StyleSheet.create(
    {Row: {height: 50}}, {header: {paddingTop: 50, paddingBottom: 20}}
);